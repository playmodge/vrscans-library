# React-UpSkill-Project.
Telerik React-UpSkill project<br/>
The goal of this project is to create a React-based load-on-demand library showing VRScans materials from a pre-defined
REST API.

## Usage

To use the app, follow these steps:

1. In the root directory of the project run: 
   `npm install` <br/>

2. To run the application use the following command
   `npm run start` <br/>
The application runs on `http://localhost:9000`

## Project structure

- `__mocks__:` - Jest framework is default supported by react for unit testing the application.
- `assets:` - as the name suggests, all the static assets should reside here
- `scss-utils` - this module holds our application-level styles
- `components:` - only shared components used across features are placed here
- `hooks` - contains some custom hooks
- `pages` - All the various features/screens/pages are defined here. In this case, “Home”, “Page1” and “Page2”
  are 3 different pages of our app.
- `redux:` - it holds all the redux resources at one place
- `sections` - all sections in the features/screens/pages
- `services` - services are tom manage all api request
- `index.js` - the entry point of the project

## Build

To get the latest build of the project, follow these steps:

1. `git checkout main`
2. `git pull`
3. `npm install`
4. `npm run start`

### Building for development
```
npm run dev
```

### Building for production
```
npm run prod
```

## Running unit tests
To run the unit test use the following command:
```
npm run test
```
If you want to run the tests with coverage:
```
npm run test:coverage
```

## Technologies & Tools Used

### Front-end
- React
- Redux
- Fortawesome
- Babel
- Webpack
- Sass
- Eslint
- Jest
- Testing-library
- Enzyme
### Back-end
- Node.js
- JSON Server

## Deploy
The project is deployed by Gitlab Pages. Here is the [url](https://playmodge.gitlab.io/vrscans-library)