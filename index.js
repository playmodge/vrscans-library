import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './src/redux/store';
import { App } from './src/App';
import { icon, library } from '@fortawesome/fontawesome-svg-core';
import {
  faUser,
  faShoppingCart,
  faPowerOff,
  faArrowAltCircleUp
} from '@fortawesome/free-solid-svg-icons';

library.add(faUser, faShoppingCart, faPowerOff, faArrowAltCircleUp);
icon({ prefix: 'fas', iconName: 'user' });
icon({ prefix: 'fas', iconName: 'shopping-cart' });
icon({ prefix: 'fas', iconName: 'power-off' });
icon({ prefix: 'fas', iconName: 'arrow-alt-circle-up' });

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
