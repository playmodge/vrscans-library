import '../AuthForm.scss';
import React from 'react';
import FormError from '@widgets/FormError';
import { useFormBindings } from '@hooks/useFormBindings';
import useAuth from '@hooks/useAuth';

export default function LoginForm() {
  const { value, setValue } = useFormBindings({
    email: '',
    password: ''
  });
  const { handleLogin, isLoadingLogin, isErrorLogin } = useAuth(value);

  return (
    <div className="auth__container">
      <div className="pen-title">
        <h1>Please login to explore our products</h1>
      </div>
      <div className="module form-module">
        <h2>Login to your account</h2>
        <div className="form">
          <FormError hasError={isErrorLogin} />
          <form onSubmit={handleLogin}>
            <input
              name="email"
              type="email"
              value={value.email}
              onChange={(e) => setValue(e)}
              placeholder="Email Address"
              disabled={isLoadingLogin}
            />
            <input
              name="password"
              type="password"
              value={value.password}
              onChange={(e) => setValue(e)}
              placeholder="Password"
              disabled={isLoadingLogin}
            />
            <button disabled={isLoadingLogin}>Sign In</button>
          </form>
        </div>
      </div>
    </div>
  );
}
