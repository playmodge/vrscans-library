import '../AuthForm.scss';
import React from 'react';
import FormError from '@widgets/FormError';
import { useFormBindings } from '@hooks/useFormBindings';
import useAuth from '@hooks/useAuth';

export default function RegisterForm() {
  const { value, setValue } = useFormBindings({
    email: '',
    password: ''
  });
  const {
    handleRegister,
    isRegisterLoading,
    isErrorRegister,
    registerErrorMessage
  } = useAuth(value);

  return (
    <div className="auth__container">
      <div className="pen-title">
        <h1>Sign up to our website</h1>
      </div>
      <div className="module form-module">
        <h2>Create an account</h2>
        <div className="form">
          <FormError hasError={isErrorRegister} message={registerErrorMessage} />
          <form onSubmit={handleRegister}>
            <input
              name="email"
              type="email"
              onChange={(e) => setValue(e)}
              placeholder="Email Address"
              disabled={isRegisterLoading}
            />
            <input
              name="password"
              type="password"
              onChange={(e) => setValue(e)}
              placeholder="Password"
              disabled={isRegisterLoading}
            />
            <button disabled={isRegisterLoading}>Sign Up</button>
          </form>
        </div>
      </div>
    </div>
  );
}
