import React from 'react';
import {shallow, mount} from 'enzyme';
import '@testing-library/jest-dom/extend-expect';
import RegisterForm from './index';

const mockTest = jest.fn();
jest.mock('react-router-dom', () => ({
  useNavigate: () => mockTest
}));

const mockTestDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useDispatch: () => mockTestDispatch
}));

const mockTestRegister = jest.fn(() => ({data: {accessToken: 'Value'}}));
jest.mock('@/service', () => ({
  useRegisterMutation: () => {
    return [mockTestRegister, {isLoading: false, isError: false, isSuccess: true}];
  }
}));

const mockTestForm = jest.fn();
jest.mock('@hooks/useFormBindings', () => ({
  useFormBindings: () => {
    return {
      value: 'Value',
      setValue: mockTestForm
    };
  }
}));

const mockTestCredentials = jest.fn();
jest.mock('@/redux/sliceReducers/authSlice', () => ({
  setCredentials: () => mockTestCredentials
}));

describe('Test rendering components', () => {
  it('should render RegisterForm component without crashing', () => {
    shallow(<RegisterForm/>);
  });
});

describe('Test <RegisterForm /> component logic', () => {
  it('should click on the email field and add text', () => {
    const wrapper = mount(<RegisterForm/>);
    const input = wrapper.find('input').at(0);
    input.simulate('change', {target: {value: 'Here'}});
    expect(mockTestForm).toHaveBeenCalled();
  });

  it('should click on the password field and add text', () => {
    const wrapper = mount(<RegisterForm/>);
    const input = wrapper.find('input').at(1);
    input.simulate('change', {target: {value: 'Here'}});
    expect(mockTestForm).toHaveBeenCalled();
  });

  it('should click two time on the RegisterForm button', async () => {
    const wrapper = mount(<RegisterForm/>);
    const input = wrapper.find('button');
    input.simulate('submit');
    expect(mockTestRegister).toHaveBeenCalled();
  });
});