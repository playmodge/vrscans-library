import './CartItem.scss';
import React from 'react';
import { manufacturers } from '@/data';
import { useDispatch } from 'react-redux';
import { removeItemFromCart } from '@/redux/sliceReducers/vrscanSlice';
import { useBuyVrscanMutation } from '@/service';
import { useFetchQuery } from '@hooks/useFetchQuery';

export default function CartItem({ item: { id, thumbnail, info } }) {
  const dispatch = useDispatch();
  const getManufacturer = manufacturers.find((m) => m.id === info.manufacturerId);

  const { refetch } = useFetchQuery();
  const [deleteItem, { isLoading }] = useBuyVrscanMutation();

  const removeFromCart = () => {
    dispatch(removeItemFromCart(id));
  };

  const buyNow = async () => {
    try {
      await deleteItem(id);
      refetch();
      dispatch(removeItemFromCart(id));
    } catch (error) {
      throw new Error(error);
    }
  };

  return (
    <div className="cart-item__container">
      <div className="cart-item__img__container">
        <img src={thumbnail} alt="cart-item" />
      </div>
      <div className="cart-item__info">
        <p className="name">{info.name}</p>
        <p className="manufacturer">{getManufacturer.name}</p>
        <p className="fileName">{info.fileName}</p>
        <button onClick={removeFromCart} disabled={isLoading} className="btn-remove">
          Remove from cart
        </button>
        <button onClick={buyNow} disabled={isLoading} className="btn-remove">
          Buy Now!
        </button>
      </div>
    </div>
  );
}
