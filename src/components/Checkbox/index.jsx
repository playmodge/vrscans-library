import React from 'react';
import './Checkbox.scss';

import { useCheckbox } from '@/hooks/useCheckbox';

export default function Checkbox({ id, label, filterType }) {
  const { value, checked, handleCheck } = useCheckbox({ id, filterType });

  return (
    <label className="checkbox__container--text">
      {label}
      <input
        type="checkbox"
        value={value}
        checked={checked}
        onChange={handleCheck}
      />
      <span className="checkmark"></span>
    </label>
  );
}
