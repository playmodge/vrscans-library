import React from 'react';
import {shallow, mount} from 'enzyme';
import '@testing-library/jest-dom/extend-expect';
import Checkbox from './index';

const mockTest = jest.fn();
jest.mock('@/hooks/useCheckbox', () => ({
  useCheckbox: () => {
    return {
      value: 'value', checked: false, handleCheck: mockTest
    };
  }
}));

describe('Test rendering components', () => {
  it('should render checkbox component without crashing', () => {
    shallow(<Checkbox id="1" label={true} filterType="materials"/>);
  });
});

describe('Test passing props to components', () => {
  it('should accept checkbox props', () => {
    const checkbox = mount(<Checkbox id="1" label={true} filterType="materials"/>);
    expect(checkbox.props().label).toEqual(true);
    expect(checkbox.props().filterType).toEqual('materials');
    expect(checkbox.props().id).toEqual('1');
  });
});

describe('Test <Checkbox /> component logic', () => {
  it('should click on the checkbox button', () => {
    const wrapper = mount(<Checkbox id="1" label={true} filterType="materials"/>);
    const input = wrapper.find('input');
    input.simulate('change', {target: {checked: true}});
    expect(mockTest).toHaveBeenCalled();
  });

  it('should click two time on the checkbox button', () => {
    const wrapper = mount(<Checkbox id="1" label={true} filterType="materials"/>);
    const input = wrapper.find('input');
    input.simulate('change', {target: {checked: true}});
    input.simulate('change', {target: {checked: false}});
    expect(mockTest).toHaveBeenCalled();
  });
});