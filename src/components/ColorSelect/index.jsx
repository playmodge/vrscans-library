import './ColorSelect.scss';
import { useCheckbox } from '@hooks/useCheckbox';

export default function ColorSelect({ id, hex, filterType }) {
  const { value, checked, handleCheck } = useCheckbox({ id, filterType });

  return (
    <div className="checkbox__container--color" style={{ backgroundColor: hex }}>
      <label>
        <input
          type="checkbox"
          value={value}
          checked={checked}
          onChange={handleCheck}
        />
        <span className="checkmark"></span>
      </label>
    </div>
  );
}
