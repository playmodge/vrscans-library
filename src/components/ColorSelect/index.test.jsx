import React from 'react';
import {shallow, mount} from 'enzyme';
import '@testing-library/jest-dom/extend-expect';
import ColorSelect from './index';

const mockTest = jest.fn();
jest.mock('@hooks/useCheckbox', () => ({
  useCheckbox: () => {
    return {
      value: 'value', checked: false, handleCheck: mockTest
    };
  }
}));

describe('Test rendering components', () => {
  it('should render checkbox component without crashing', () => {
    shallow(<ColorSelect id="1" hex={true} filterType="colors"/>);
  });
});

describe('Test passing props to components', () => {
  it('should accept ColorSelect props', () => {
    const colorSelect = mount(<ColorSelect id="1" hex={true} filterType="colors"/>);
    expect(colorSelect.props().hex).toEqual(true);
    expect(colorSelect.props().filterType).toEqual('colors');
    expect(colorSelect.props().id).toEqual('1');
  });
});

describe('Test <ColorSelect /> component logic', () => {
  it('should click on the ColorSelect button', () => {
    const wrapper = mount(<ColorSelect id="1" hex={true} filterType="colors"/>);
    const input = wrapper.find('input');
    input.simulate('change', {target: {checked: true}});
    expect(mockTest).toHaveBeenCalled();
  });

  it('should click two time on the ColorSelect button', () => {
    const wrapper = mount(<ColorSelect id="1" hex={true} filterType="colors"/>);
    const input = wrapper.find('input');
    input.simulate('change', {target: {checked: true}});
    input.simulate('change', {target: {checked: false}});
    expect(mockTest).toHaveBeenCalled();
  });
});


