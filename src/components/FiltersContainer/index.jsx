import './FiltersContainer.scss';

export default function FiltersContainer({ label, children, wrapItems }) {
  return (
    <div className="filters__container">
      <p className="filters__container-label">{label}</p>
      <div style={wrapItems && { display: 'flex', flexWrap: 'wrap' }}>
        {children}
      </div>
    </div>
  );
}
