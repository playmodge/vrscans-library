import React from 'react';
import {shallow, mount} from 'enzyme';
import '@testing-library/jest-dom/extend-expect';
import FiltersContainer from './index';
import Checkbox from '../Checkbox';

const mockTest = jest.fn();
jest.mock('@/hooks/useCheckbox', () => ({
  useCheckbox: () => {
    return {
      value: 'value', checked: false, handleCheck: mockTest
    };
  }
}));

describe('Test rendering components without token', () => {
  it('should render FiltersContainer component without crashing', () => {
    shallow(
      <FiltersContainer label="material type">
        <Checkbox key="1" id="1" label="{m.name}" filterType="materials"/>
      </FiltersContainer>
    );
  });

  it('"should render <Checkbox /> component without crashing"', () => {
    const wrapper = shallow(
      <FiltersContainer label="material type">
        <Checkbox key="1" id="1" label="{m.name}" filterType="materials"/>
      </FiltersContainer>
    );
    expect(wrapper.contains(<Checkbox key="1" id="1" label="{m.name}" filterType="materials"/>)).toEqual(true);
  });
});

describe('Test passing props to components', () => {
  it('should accept FiltersContainer props', () => {
    const wrapper = mount(
      <FiltersContainer label="material type">
        <Checkbox key="1" id="1" label="{m.name}" filterType="materials"/>
      </FiltersContainer>
    );
    expect(wrapper.props().label).toEqual('material type');
  });
});

