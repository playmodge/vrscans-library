import './NotFound.scss';

export default function NotFound() {
  return (
    <div className="not-found__container">
      <div className="not-found__content">
        <h1>404</h1>
        <h2>Looks like the page you were looking for is no longer here.</h2>
      </div>
    </div>
  );
}
