import React from 'react';
import {shallow, mount} from 'enzyme';
import '@testing-library/jest-dom/extend-expect';
import NotFound from './index';

describe('Test rendering components', () => {
  it('should render NotFound component without crashing', () => {
    shallow(<NotFound/>);
  });
});

