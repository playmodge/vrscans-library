import { useSelector } from 'react-redux';
import { Navigate, Outlet } from 'react-router-dom';

export default function PrivateRoute() {
  const authToken = useSelector((state) => state.auth.accessToken);
  return authToken ? <Outlet /> : <Navigate to="/login" />;
}
