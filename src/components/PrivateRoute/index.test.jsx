import React from 'react';
import {shallow, mount} from 'enzyme';
import '@testing-library/jest-dom/extend-expect';
import PrivateRoute from './index';
import { Navigate, Outlet } from 'react-router-dom';

describe('Test rendering components without token', () => {
  it('should render PrivateRoute component without crashing', () => {
    const wrapper = shallow(<PrivateRoute/>);
    expect(wrapper.contains(<Navigate to="/login"/>)).toEqual(true);
  });

  it('should render PrivateRoute component without crashing with token', () => {
    Storage.prototype.getItem = jest.fn(() => 'Something');
    const wrapper = shallow(<PrivateRoute/>);
    expect(wrapper.contains(<Outlet />)).toEqual(true);
  });
});

