import './Product.scss';
import ProductInfo from '../ProductInfo';
import { useDispatch } from 'react-redux';
import { setVrscansCart } from '@/redux/sliceReducers/vrscanSlice';

export default function Product({ id, thumbnail, info }) {
  const dispatch = useDispatch();
  const addToCart = () => {
    dispatch(setVrscansCart({ id, thumbnail, info }));
  };
  return (
    <div className="product">
      <img src={thumbnail} />
      <ProductInfo info={info} />
      <button onClick={addToCart} className="btn-add">
        Add to cart
      </button>
    </div>
  );
}
