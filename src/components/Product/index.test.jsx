import React from 'react';
import {shallow, mount} from 'enzyme';
import '@testing-library/jest-dom/extend-expect';
import Product from './index';
import ProductInfo from '../ProductInfo';

const vs = {
  id: 1708,
  name: 'Lin69',
  thumb: 'https://download.chaosgroup.com/images/vrscans/thumb/lin69',
  fileName: 'lin69.vrscan',
  manufacturerId: 5,
};

const mockTestDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useDispatch: () => mockTestDispatch
}));

const mockTest = jest.fn();
jest.mock('@/redux/sliceReducers/vrscanSlice', () => ({
  setVrscansCart: () => mockTest
}));

describe('Test rendering components', () => {
  it('should render product component without crashing', () => {
    shallow(<Product thumbnail={vs.thumb}
                     info={{
                       manufacturerId: vs.manufacturerId,
                       name: vs.name,
                       fileName: vs.fileName
                     }}
                     key={vs.id}/>
    );
  });

  it('should render <ProductInfo /> component without crashing', () => {
    const wrapper = shallow(<Product thumbnail={vs.thumb}
                                     info={{
                                       manufacturerId: vs.manufacturerId,
                                       name: vs.name,
                                       fileName: vs.fileName
                                     }}
                                     key={vs.id}/>);

    expect(wrapper.contains(<ProductInfo info={{
      manufacturerId: vs.manufacturerId,
      name: vs.name,
      fileName: vs.fileName
    }}/>)).toEqual(true);
  });


});

describe('Test passing props to components', () => {
  const expectedText = {
    manufacturerId: vs.manufacturerId,
    name: vs.name,
    fileName: vs.fileName
  };
  it('should accept Product props', () => {
    const checkbox = mount(<Product thumbnail={vs.thumb}
                                    info={expectedText}
                                    key={vs.id}/>);
    expect(checkbox.props().thumbnail).toEqual('https://download.chaosgroup.com/images/vrscans/thumb/lin69');
    expect(checkbox.props().info).toEqual(expectedText);
  });

  it('should accept ProductInfo props', () => {
    const info = mount(<ProductInfo info={expectedText}/>);
    expect(info.props().info).toEqual(expectedText);
  });
});

