import './ProductInfo.scss';
import { manufacturers } from '../../data';

export default function ProductInfo({ info }) {
  const getManufacturer = manufacturers.find((m) => m.id === info.manufacturerId);
  return (
    <div className="product-info">
      <p className="name">{info.name}</p>
      <p className="manufacturer">{getManufacturer.name}</p>
      <p className="fileName">{info.fileName}</p>
    </div>
  );
}

