import React from 'react';
import {shallow, mount} from 'enzyme';
import '@testing-library/jest-dom/extend-expect';
import ProductInfo from '../ProductInfo';

const info = {
  manufacturerId: 5, name: 'name', fileName: 'fileName'
};

describe('Test rendering components', () => {
  it('should render checkbox component without crashing', () => {
    shallow(<ProductInfo info={info}/>);
  });
});

describe('Test passing props to components', () => {
  it('should accept ColorSelect props', () => {
    const productInfo = mount(<ProductInfo info={info}/>);
    expect(productInfo.props().info).toEqual(info);
  });
});
