import './ProductsGallery.scss';
import Product from '../Product';
import SearchBar from '@/components/SearchBar';
import { useFetchQuery } from '@hooks/useFetchQuery';
import { useSelector } from 'react-redux';
import GlobalLoader from '@widgets/GlobalLoader';

export default function ProductsGallery() {
  const vrscans = useSelector((state) => state.vrscan.items);
  const { data, page, setPage, isFetching } = useFetchQuery();
  const handleLoadMore = () => setPage(page + 1);

  return (
    <div className="products__container">
      <SearchBar />
      <div className="products-gallery">
        {!vrscans.length && !isFetching && <h2>There are no available items!</h2>}
        {!vrscans.length && isFetching && <GlobalLoader />}
        {vrscans.length > 0 &&
          vrscans.map((vs) => (
            <Product
              id={vs.id}
              thumbnail={vs.thumb}
              info={{
                manufacturerId: vs.manufacturerId,
                name: vs.name,
                fileName: vs.fileName
              }}
              key={vs.id}
            />
          ))}
      </div>
      {vrscans.length > 0 && data.length > 0 && (
        <button
          className="btn-load-more"
          disabled={isFetching}
          onClick={handleLoadMore}
        >
          Load More
        </button>
      )}
    </div>
  );
}
