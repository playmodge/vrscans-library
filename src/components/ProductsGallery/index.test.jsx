import React from 'react';
import {shallow, mount} from 'enzyme';
import '@testing-library/jest-dom/extend-expect';
import ProductsGallery from './index';
import SearchBar from '../SearchBar';

const mockTest = jest.fn();

jest.mock('@hooks/useFetchQuery', () => ({
  useFetchQuery: () => {
    return {
      page: 1, setPage: mockTest
    };
  }
}));

const mockTestSearch = jest.fn();

jest.mock('@hooks/useSearch', () => ({
  useSearch: () => {
    return [mockTestSearch];
  }
}));

const mockTestCart = jest.fn();
jest.mock('@/redux/sliceReducers/vrscanSlice', () => ({
  setVrscansCart: () => mockTestCart
}));

const vs = {
  id: 1708,
  name: 'Lin69',
  thumb: 'https://download.chaosgroup.com/images/vrscans/thumb/lin69',
  fileName: 'lin69.vrscan',
  manufacturerId: 5,
};

jest.mock('react-redux', () => ({
  useSelector: () => {
    return {
      state: [vs]
    };
  }
}));

describe('Test rendering components', () => {
  it('should render ProductsGallery component without crashing', () => {
    shallow(<ProductsGallery/>);
  });
  it('should render <SearchBar /> component without crashing', () => {
    const wrapper = shallow(<ProductsGallery/>);

    expect(wrapper.contains(<SearchBar/>)).toEqual(true);
  });
});


describe('Test <ProductsGallery /> component logic', () => {
  it('should click on the ProductsGallery button', () => {
    const wrapper = mount(<ProductsGallery/>);
    const input = wrapper.find('button');
    input.simulate('click');
    expect(mockTest).toHaveBeenCalled();
  });

  it('should click two time on the ProductsGallery button', () => {
    const wrapper = mount(<ProductsGallery/>);
    const input = wrapper.find('button');
    input.simulate('click');
    expect(mockTest).toHaveBeenCalled();
    input.simulate('click');
    expect(mockTest).toHaveBeenCalled();
  });
});


