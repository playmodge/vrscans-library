import './SearchBar.scss';
import React from 'react';
import _ from 'lodash';
import useSearch from '@hooks/useSearch';

export default function SearchBar() {
  const [value, handleSearch] = useSearch();
  return (
    <div className="searchbar__container">
      <input
        type="text"
        onChange={handleSearch}
        value={value}
        placeholder="Search.."
        name="search"
      />
    </div>
  );
}
