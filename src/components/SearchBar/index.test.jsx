import React from 'react';
import {shallow, mount} from 'enzyme';
import '@testing-library/jest-dom/extend-expect';
import SearchBar from './index';

const mockTest = jest.fn();

jest.mock('@hooks/useSearch', () => ({
  useSearch: () => {
    return [mockTest];
  }
}));

jest.mock('react-redux', () => ({
  useSelector: () => {
    return {
      state: 'Something'
    };
  }
}));

describe('Test rendering components', () => {
  it('should render SearchBar component without crashing', () => {
    shallow(<SearchBar/>);
  });
});

describe('Test <SearchBar /> component logic', () => {
  it('should click on the SearchBar button', () => {
    const wrapper = mount(<SearchBar/>);
    const input = wrapper.find('input');
    input.simulate('change', {target: {value: 'Here'}});
    expect(mockTest).toHaveBeenCalled();
  });

  it('should click two time on the SearchBar button', () => {
    const wrapper = mount(<SearchBar/>);
    const input = wrapper.find('input');
    input.simulate('change', {value: 'Here'});
    expect(mockTest).toHaveBeenCalled();
    input.simulate('change', {value: 'Here'});
    expect(mockTest).toHaveBeenCalled();
  });
});


