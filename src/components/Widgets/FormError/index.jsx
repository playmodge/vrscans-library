import './FormError.scss';

export default function FormError({ hasError, message }) {
  return (
    hasError && (
      <p className="form-error">{message ? message : 'Invalid credentials'}</p>
    )
  );
}
