import './GlobalLoader.scss';

export default function GlobalLoader() {
  return (
    <div className="global-loader__container">
      <div className="global-loader__ring">
        Loading
        <span></span>
      </div>
    </div>
  );
}
