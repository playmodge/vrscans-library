import './ScrollToTop.scss';
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import useScrollToTop from '@hooks/useScrollTop';

export default function ScrollToTop() {
  const { isVisible, scrollToTop } = useScrollToTop(false);

  return (
    <>
      {isVisible && (
        <FontAwesomeIcon
          onClick={scrollToTop}
          className="btn-scroll-to-top"
          icon="arrow-alt-circle-up"
        />
      )}
    </>
  );
}
