import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useLoginMutation, useRegisterMutation } from '@/service';
import { setCredentials } from '@/redux/sliceReducers/authSlice';

export default function useAuth(value) {
  const userIsLoggedIn = useSelector((state) => state.auth.accessToken);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [login, { isLoading: isLoadingLogin, isError: isErrorLogin }] =
    useLoginMutation();
  const [register, { isLoading: isRegisterLoading, isError: isErrorRegister }] =
    useRegisterMutation();

  const [registerErrorMessage, setRegisterErrorMessage] = React.useState('');

  React.useEffect(() => {
    if (userIsLoggedIn) {
      navigate('/');
    }
  }, []);

  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      const res = await login(value);
      dispatch(setCredentials(res.data.accessToken));
      navigate('/');
    } catch (error) {
      throw new Error(error);
    }
  };

  const handleRegister = async (e) => {
    e.preventDefault();
    try {
      const res = await register(value);
      if (res.error) {
        setRegisterErrorMessage(res.error.data);
      } else {
        dispatch(setCredentials(res.data.accessToken));
        navigate('/');
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  return {
    handleLogin,
    handleRegister,
    isLoadingLogin,
    isErrorLogin,
    isRegisterLoading,
    isErrorRegister,
    registerErrorMessage
  };
}
