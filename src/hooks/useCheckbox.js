import React from 'react';
import { useDispatch } from 'react-redux';
import { setFilters } from '@/redux/sliceReducers/vrscanSlice';

export function useCheckbox({ id, filterType }) {
  const dispatch = useDispatch();
  const [value, setValue] = React.useState('');
  const [previousValue, setPreviousValue] = React.useState('');

  const [checked, setChecked] = React.useState(false);

  const handleCheck = (e) => setChecked(e.target.checked);

  React.useEffect(() => {
    if (checked) {
      setValue(id);
    } else {
      setPreviousValue(value);
      setValue('');
    }
  }, [checked]);

  React.useEffect(() => {
    dispatch(setFilters({ type: filterType, value, checked, previousValue }));
  }, [value]);

  return { value, checked, handleCheck };
}
