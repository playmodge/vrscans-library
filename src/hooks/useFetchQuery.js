import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useVrscansQuery } from '@/service';
import { setVrscans, resetVrscans } from '@/redux/sliceReducers/vrscanSlice';

export const useFetchQuery = () => {
  const [page, setPage] = React.useState(1);

  const dispatch = useDispatch();
  const { materials, colors, tags } = useSelector((state) => state.vrscan.filters);
  const searchText = useSelector((state) => state.vrscan.searchText);

  const buildQuery = (page, materials, colors, tags, search) => {
    let url = '';
    if (materials !== undefined && materials.length > 0)
      materials.forEach((material) => (url += `&materialTypeId=${material}`));
    if (tags !== undefined && tags.length > 0)
      tags.forEach((tag) => (url += `&tags=${tag}&`));
    if (colors !== undefined && colors.length > 0)
      colors.forEach((color) => (url += `&colors=${color}&`));
    if (search) url += `&${url}q=${search}`;
    return `vrscans?_page=${page}&_limit=16${url || ''}`;
  };

  const { data, isLoading, isFetching, refetch } = useVrscansQuery(
    buildQuery(page, materials, colors, tags, searchText)
  );

  React.useEffect(() => {
    dispatch(resetVrscans());
    setPage(1);
  }, [materials, colors, tags, searchText]);

  React.useEffect(() => {
    if (data) {
      dispatch(setVrscans(data));
    }
  }, [data]);

  return { data, page, setPage, isLoading, isFetching, refetch };
};
