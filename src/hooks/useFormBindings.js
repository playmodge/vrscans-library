import React from 'react';

export const useFormBindings = (initialValue) => {
  const [value, handleValue] = React.useState(initialValue);

  const setValue = React.useCallback((e) => {
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    handleValue((prevState) => ({
      ...prevState,
      [e.target.name]: value
    }));
  }, []);

  return { value, setValue };
};
