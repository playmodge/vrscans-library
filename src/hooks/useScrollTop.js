import React from 'react';
import useIsMounted from './useIsMounted';

export default function useScrollTop() {
  const [isVisible, setIsVisible] = React.useState(false);
  const isMounted = useIsMounted();

  const toggleVisible = React.useCallback(() => {
    const scrolled = document.documentElement.scrollTop;

    if (scrolled > 30) {
      isMounted() && setIsVisible(true);
    } else if (scrolled <= 30) {
      isMounted() && setIsVisible(false);
    }
  }, []);

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  };

  React.useEffect(() => {
    window.addEventListener('scroll', toggleVisible);
    return () => {
      window.removeEventListener('scroll', null);
    };
  }, []);
  return { isVisible, scrollToTop };
}
