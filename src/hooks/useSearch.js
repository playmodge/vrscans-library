import React from 'react';
import _ from 'lodash';
import { useDispatch } from 'react-redux';
import { setSearchText } from '@/redux/sliceReducers/vrscanSlice';

export default function useSearch() {
  const dispatch = useDispatch();
  const [value, setValue] = React.useState('');

  const setDebounce = _.debounce(function (v) {
    dispatch(setSearchText(v));
  }, 800);

  const debounceSearch = React.useCallback((v) => {
    setDebounce(v);
  }, []);

  const handleChange = (e) => {
    const value = e.target.value;
    setValue(value);
    debounceSearch(value);
  };

  React.useEffect(() => {
    return () => {
      dispatch(setSearchText(''));
    };
  }, []);

  return [value, handleChange];
}
