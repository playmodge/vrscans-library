import './Cart.scss';
import CartItem from '@/components/CartItem';
import ScrollToTop from '@widgets/ScrollToTop';
import { useSelector } from 'react-redux';

export default function Cart() {
  const itemsInCart = useSelector((state) => state.vrscan.itemsInCart);
  return (
    <div className="cart">
      {itemsInCart.length > 0 ? (
        itemsInCart.map((item) => <CartItem item={item} key={item.id} />)
      ) : (
        <h2>There are no items in cart</h2>
      )}
      <ScrollToTop />
    </div>
  );
}
