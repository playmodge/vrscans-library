import './Dashboard.scss';
import Sidebar from '@/sections/Sidebar';
import Main from '@/sections/Main';
import ScrollToTop from '@widgets/ScrollToTop';
import React from 'react';

export default function Dashboard() {
  return (
    <div className="dashboard">
      <Sidebar />
      <Main />
      <ScrollToTop />
    </div>
  );
}
