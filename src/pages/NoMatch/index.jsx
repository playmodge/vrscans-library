import './Wildcard.scss';
import NotFound from '../../components/NotFound';

export default function Wildcard() {
  return (
    <div className="wildcard">
      <NotFound />
    </div>
  );
}
