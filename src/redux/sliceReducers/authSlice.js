import { createSlice } from '@reduxjs/toolkit';

const authSlice = createSlice({
  name: 'auth',
  initialState: { accessToken: localStorage.getItem('accessToken') },
  reducers: {
    setCredentials(state, { payload }) {
      if (payload) {
        localStorage.setItem('accessToken', payload);
        state.accessToken = payload;
      }
    },
    removeCredentials(state) {
      localStorage.setItem('accessToken', '');
      state.accessToken = '';
    }
  }
});
export const { setCredentials, removeCredentials } = authSlice.actions;

export default authSlice.reducer;
