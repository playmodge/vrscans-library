import { createSlice } from '@reduxjs/toolkit';
import { current } from '@reduxjs/toolkit';

const vrscanSlice = createSlice({
  name: 'vrscan',
  initialState: {
    items: [],
    itemsInCart: [],
    filters: {
      materials: [],
      colors: [],
      tags: []
    },
    searchText: ''
  },
  reducers: {
    resetVrscans(state) {
      state.items = [];
    },
    setSearchText(state, { payload }) {
      state.searchText = payload;
    },
    setVrscans(state, { payload }) {
      if (payload) {
        const copyState = current(state);
        state.items = copyState.items.concat(payload);
      }
    },
    setVrscansCart(state, { payload }) {
      const copyState = current(state);
      const hasAlreadyItemInCart = copyState.itemsInCart.find(
        (item) => item.id === payload.id
      );
      !hasAlreadyItemInCart && state.itemsInCart.push(payload);
    },
    setFilters(state, { payload: { type, value, previousValue, checked } }) {
      const previousValueIndex = state.filters[type].indexOf(previousValue);
      checked
        ? state.filters[type].push(value)
        : state.filters[type].splice(previousValueIndex, 1);
    },
    removeItemFromCart(state, { payload }) {
      const copyState = current(state);
      state.itemsInCart = copyState.itemsInCart.filter(
        (item) => item.id !== payload
      );
    }
  }
});

export const {
  setVrscans,
  setFilters,
  resetVrscans,
  setSearchText,
  setSearchTextRealtime,
  setVrscansCart,
  removeItemFromCart
} = vrscanSlice.actions;

export default vrscanSlice.reducer;
