import { configureStore } from '@reduxjs/toolkit';
import authReducer from '@/redux/sliceReducers/authSlice';
import vrscanReducer from '@/redux/sliceReducers/vrscanSlice';
import { api } from '@/service';

export const store = configureStore({
  reducer: {
    auth: authReducer,
    vrscan: vrscanReducer,
    [api.reducerPath]: api.reducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(api.middleware)
});
