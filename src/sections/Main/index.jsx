import './Main.scss';
import ProductsGallery from '@/components/ProductsGallery';

export default function Main() {
  return (
    <div className="main">
      <ProductsGallery />
    </div>
  );
}
