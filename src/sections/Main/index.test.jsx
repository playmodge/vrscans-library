import React from 'react';
import {shallow, mount} from 'enzyme';
import '@testing-library/jest-dom/extend-expect';
import Main from './index';

const mockTest = jest.fn();

jest.mock('@hooks/useFetchQuery', () => ({
  useFetchQuery: () => {
    return {
      page: 1, setPage: mockTest
    };
  }
}));

const mockTestSearch = jest.fn();

jest.mock('@hooks/useSearch', () => ({
  useSearch: () => {
    return [mockTestSearch];
  }
}));

const vs = {
  id: 1708,
  name: 'Lin69',
  thumb: 'https://download.chaosgroup.com/images/vrscans/thumb/lin69',
  fileName: 'lin69.vrscan',
  manufacturerId: 5,
};

jest.mock('react-redux', () => ({
  useSelector: () => {
    return {
      state: [vs]
    };
  }
}));

const mockTestCart = jest.fn();
jest.mock('@/redux/sliceReducers/vrscanSlice', () => ({
  setVrscansCart: () => mockTestCart
}));

describe('Test rendering components', () => {
  it('should render Main component without crashing', () => {
    shallow(<Main/>);
  });
});


