import './Navigation.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { removeCredentials } from '@/redux/sliceReducers/authSlice';

export default function Navigation() {
  const dispatch = useDispatch();
  const userIsLoggedIn = useSelector((state) => state.auth.accessToken);
  const cartItems = useSelector((state) => state.vrscan.itemsInCart);
  const navigate = useNavigate();
  const logout = () => {
    dispatch(removeCredentials());
    navigate('/login');
  };

  return (
    <div className="navigation">
      {userIsLoggedIn ? (
        <div>
          <NavLink to="/">Home</NavLink>
          <NavLink to="/cart">
            Go to cart{' '}
            <FontAwesomeIcon
              icon="shopping-cart"
              className={`${
                cartItems.length && 'shopping-cart--active'
              } shopping-cart`}
            />
            <span className="shopping-cart__count-items">{cartItems.length}</span>
          </NavLink>
          <FontAwesomeIcon
            className="btn-icon__logout"
            onClick={logout}
            icon="power-off"
          />
        </div>
      ) : (
        <div>
          <NavLink to="/login">Login</NavLink>
          <NavLink to="/register">Register</NavLink>
        </div>
      )}
    </div>
  );
}
