import './Sidebar.scss';
import { materials, colors, tags } from '@/data';
import FiltersContainer from '@/components/FiltersContainer';
import Checkbox from '@/components/Checkbox';
import ColorSelect from '@/components/ColorSelect';

export default function Sidebar() {
  return (
    <div className="sidebar">
      <FiltersContainer label="material type">
        {materials.map((m) => (
          <Checkbox key={m.id} id={m.id} label={m.name} filterType="materials" />
        ))}
      </FiltersContainer>
      <FiltersContainer label="material color" wrapItems>
        {colors.map((c) => (
          <ColorSelect key={c.id} id={c.id} hex={c.hex} filterType="colors" />
        ))}
      </FiltersContainer>
      <FiltersContainer label="tags">
        {tags.map((t) => (
          <Checkbox key={t.id} id={t.id} label={t.name} filterType="tags" />
        ))}
      </FiltersContainer>
    </div>
  );
}
