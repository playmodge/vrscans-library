import React from 'react';
import {shallow, mount} from 'enzyme';
import '@testing-library/jest-dom/extend-expect';
import Sidebar from './index';

const mockTest = jest.fn();
jest.mock('@/hooks/useCheckbox', () => ({
  useCheckbox: () => {
    return {
      value: 'value', checked: false, handleCheck: mockTest
    };
  }
}));

describe('Test rendering components', () => {
  it('should render Sidebar component without crashing', () => {
    shallow(<Sidebar/>);
  });
});


