import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const api = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({
    baseUrl: 'https://vrscansdb.herokuapp.com/',
    prepareHeaders: (headers, { getState }) => {
      const token = getState().auth.accessToken;
      headers.set('Authorization', `Bearer ${token}`);
      return headers;
    }
  }),
  endpoints: (builder) => ({
    login: builder.mutation({
      query: (credentials) => ({
        url: 'login',
        method: 'POST',
        body: credentials
      })
    }),
    register: builder.mutation({
      query: (credentials) => ({
        url: 'register',
        method: 'POST',
        body: credentials
      })
    }),
    vrscans: builder.query({
      query: (url) => url
    }),
    buyVrscan: builder.mutation({
      query: (id) => ({
        url: `vrscans/${id}`,
        method: 'DELETE'
      })
    })
  })
});

export const {
  useLoginMutation,
  useRegisterMutation,
  useVrscansQuery,
  useBuyVrscanMutation
} = api;
